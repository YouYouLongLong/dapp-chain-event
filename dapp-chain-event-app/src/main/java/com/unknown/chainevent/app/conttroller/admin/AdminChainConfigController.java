package com.unknown.chainevent.app.conttroller.admin;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.unknow.first.api.common.CommonPage;
import com.unknow.first.api.common.CommonParam;
import com.unknown.chainevent.biz.service.ChainConfigService;
import com.unknown.chainevent.common.entity.ChainConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.cloud.constant.CoreConstant;
import org.cloud.dimension.annotation.SystemResource;
import org.cloud.exception.BusinessException;
import org.cloud.vo.CommonApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "后台管理-链配置管理", tags = "AdminChainConfigController")
@Validated
@RestController
@RequestMapping("/admin/chain")
@SystemResource(path = "/admin/chain")
@Slf4j
public class AdminChainConfigController {

    @Autowired
    private ChainConfigService chainConfigService;

    @ApiOperation("获取链配置")
    @SystemResource(value = "/admin-getChainList", description = "获取链配置", authMethod = CoreConstant.AuthMethod.BYUSERPERMISSION)
    @RequestMapping(method = RequestMethod.POST, value = "/getChainList")
    public CommonApiResult<CommonPage<ChainConfig>> getChainList(CommonParam pageParam) throws BusinessException {
        try(Page<?> ignored = PageHelper.startPage(pageParam.getPage(), pageParam.getLimit(), pageParam.getSorts())){
            List<ChainConfig> list = chainConfigService.list();
            return CommonApiResult.createSuccessResult(CommonPage.restPage(list));
        }
    }

    @ApiOperation("编辑链配置")
    @SystemResource(value = "/admin-updateChain", description = "编辑链配置", authMethod = CoreConstant.AuthMethod.BYUSERPERMISSION)
    @RequestMapping(method = RequestMethod.POST, value = "/updateChain")
    public CommonApiResult<?>  updateChain(@RequestBody ChainConfig config) throws BusinessException {
        try {
            return chainConfigService.updateChain(config);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(e.getMessage());
        }
    }

    @ApiOperation("添加链配置")
    @SystemResource(value = "/admin-addChain", description = "添加链配置", authMethod = CoreConstant.AuthMethod.BYUSERPERMISSION)
    @RequestMapping(method = RequestMethod.POST, value = "/addChain")
    public CommonApiResult<?>  addChain(@RequestBody ChainConfig config) throws BusinessException {
        try {
            return chainConfigService.addChain(config);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(e.getMessage());
        }
    }

    @ApiOperation("启用禁用")
    @SystemResource(value = "/admin-updateStatus", description = "启用禁用", authMethod = CoreConstant.AuthMethod.BYUSERPERMISSION)
    @RequestMapping(method = RequestMethod.POST, value = "/updateStatus")
    public CommonApiResult<?>  updateStatus(Long chainConfigId, Integer status) throws BusinessException {
        try {
            return chainConfigService.updateStatus(chainConfigId,status);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(e.getMessage());
        }
    }
}
