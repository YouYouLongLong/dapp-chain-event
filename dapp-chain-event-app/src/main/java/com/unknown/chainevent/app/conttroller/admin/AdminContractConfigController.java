package com.unknown.chainevent.app.conttroller.admin;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.unknow.first.api.common.CommonPage;
import com.unknow.first.api.common.CommonParam;
import com.unknown.chainevent.biz.service.ContractConfigService;
import com.unknown.chainevent.common.entity.ContractConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.cloud.constant.CoreConstant;
import org.cloud.dimension.annotation.SystemResource;
import org.cloud.exception.BusinessException;
import org.cloud.vo.CommonApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@Api(value = "后台管理-合约配置管理", tags = "AdminContractConfigController")
@Validated
@RestController
@RequestMapping("/admin/contract")
@SystemResource(path = "/admin/contract")
@Slf4j
public class AdminContractConfigController {

    @Autowired
    private ContractConfigService contractConfigService;




    @ApiOperation("获取合约配置")
    @SystemResource(value = "/admin-getContractList", description = "获取合约配置", authMethod = CoreConstant.AuthMethod.BYUSERPERMISSION)
    @RequestMapping(method = RequestMethod.POST, value = "/getContractList")
    public CommonApiResult<CommonPage<ContractConfig>> getContractList(CommonParam pageParam, String contractAddress, Integer contractType)
        throws BusinessException {

        QueryWrapper<ContractConfig> where = new QueryWrapper<>();
        where.eq(!ObjectUtils.isEmpty(contractAddress), "contract_address", contractAddress);
        where.eq(!ObjectUtils.isEmpty(contractType), "contract_type", contractType);
        try(Page<?> ignored = PageHelper.startPage(pageParam.getPage(), pageParam.getLimit(), pageParam.getSorts())){
            List<ContractConfig> list = contractConfigService.list();
            return CommonApiResult.createSuccessResult(CommonPage.restPage(list));
        }
    }

    @ApiOperation("编辑合约配置")
    @SystemResource(value = "/admin-updateContract", description = "编辑合约配置", authMethod = CoreConstant.AuthMethod.BYUSERPERMISSION)
    @RequestMapping(method = RequestMethod.POST, value = "/updateContract")
    public CommonApiResult<?> updateContract(@RequestBody ContractConfig config) throws BusinessException {
        try {
            return contractConfigService.updateContract(config);
        } catch (Exception e) {
            log.error("{}", e);
            throw new BusinessException(e.getMessage());
        }
    }

    @ApiOperation("添加合约配置")
    @SystemResource(value = "/admin-addContract", description = "添加合约配置", authMethod = CoreConstant.AuthMethod.BYUSERPERMISSION)
    @RequestMapping(method = RequestMethod.POST, value = "/addContract")
    public CommonApiResult<?> addContract(@RequestBody ContractConfig config) throws BusinessException {
        try {
            return contractConfigService.addContract(config);
        } catch (Exception e) {
            log.error("{}", e);
            throw new BusinessException(e.getMessage());
        }
    }

    @ApiOperation("启用禁用")
    @SystemResource(value = "/admin-updateStatus", description = "启用禁用", authMethod = CoreConstant.AuthMethod.BYUSERPERMISSION)
    @RequestMapping(method = RequestMethod.POST, value = "/updateStatus")
    public CommonApiResult<?> updateStatus(Long contractConfigId, Integer status) throws BusinessException {
        try {
            return contractConfigService.updateStatus(contractConfigId, status);
        } catch (Exception e) {
            log.error("{}", e);
            throw new BusinessException(e.getMessage());
        }
    }


}
