package com.unknown.chainevent.app.conttroller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.unknow.first.api.common.CommonPage;
import com.unknow.first.api.common.CommonParam;
import com.unknown.chainevent.app.dto.query.ContractLogsQueryParamDTO;
import com.unknown.chainevent.biz.service.ContractLogsService;
import com.unknown.chainevent.common.entity.ContractLogs;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.cloud.constant.CoreConstant;
import org.cloud.dimension.annotation.SystemResource;
import org.cloud.mybatisplus.utils.MyBatisPlusUtil;
import org.cloud.vo.CommonApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "后台管理-事件管理", tags = "AdminContractLogsController")
@Validated
@RestController
@RequestMapping("/admin/contract/logs")
@SystemResource(path = "/admin/contract/logs")
@Slf4j
public class AdminContractLogsController {

    @Autowired
    private ContractLogsService contractLogsService;

    @ApiOperation("获取事件列表")
    @SystemResource(value = "/list", description = "获取事件列表", authMethod = CoreConstant.AuthMethod.BYUSERPERMISSION)
    @RequestMapping(method = RequestMethod.POST, value = "/list")
    public CommonApiResult<CommonPage<ContractLogs>> list(CommonParam pageParam, ContractLogsQueryParamDTO queryParamDTO){

        QueryWrapper<ContractLogs> queryWrapper = MyBatisPlusUtil.single().getPredicate(queryParamDTO);
        PageHelper.startPage(pageParam.getPage(), pageParam.getLimit(), ObjectUtils.isEmpty(pageParam.getSorts()) ? "id desc" : pageParam.getSorts());
        List<ContractLogs> logsList = contractLogsService.list(queryWrapper);
        return CommonApiResult.createSuccessResult(CommonPage.restPage(logsList));

    }
}
