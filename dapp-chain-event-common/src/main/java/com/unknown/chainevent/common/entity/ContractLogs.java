package com.unknown.chainevent.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 转账记录日志表
 */
@ApiModel(description = "转账记录日志表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_contract_logs")
public class ContractLogs {
    @TableId(value = "contract_log_id", type = IdType.AUTO)
    @ApiModelProperty(value = "")
    private Long contractLogId;

    @TableField(value = "chain_config_id")
    @ApiModelProperty(value = "")
    private Long chainConfigId;

    /**
     * 链符号
     */
    @TableField(value = "chain_symbol")
    @ApiModelProperty(value = "链符号")
    private String chainSymbol;

    /**
     * 合约配合id
     */
    @TableField(value = "contract_config_id")
    @ApiModelProperty(value = "合约配合id")
    private Long contractConfigId;

    /**
     * 事件所在合约地址
     */
    @TableField(value = "contract_address")
    @ApiModelProperty(value = "事件所在合约地址")
    private String contractAddress;

    /**
     * 日志所在交易hash
     */
    @TableField(value = "trans_hash")
    @ApiModelProperty(value = "日志所在交易hash")
    private String transHash;

    /**
     * 日志序号（相对区块）
     */
    @TableField(value = "index_for_block")
    @ApiModelProperty(value = "日志序号（相对区块）")
    private Long indexForBlock;

    /**
     * 原始交易发起方
     */
    @TableField(value = "trans_from")
    @ApiModelProperty(value = "原始交易发起方")
    private String transFrom;

    /**
     * 原始交易调用的合约
     */
    @TableField(value = "trans_to")
    @ApiModelProperty(value = "原始交易调用的合约")
    private String transTo;

    /**
     * 类型，暂时不知道何用
     */
    @TableField(value = "`type`")
    @ApiModelProperty(value = "类型，暂时不知道何用")
    private String type;

    /**
     * topic0，代表日志名字
     */
    @TableField(value = "first_topic")
    @ApiModelProperty(value = "topic0，代表日志名字")
    private String firstTopic;

    /**
     * topic1
     */
    @TableField(value = "second_topic")
    @ApiModelProperty(value = "topic1")
    private String secondTopic;

    /**
     * topic2
     */
    @TableField(value = "third_topic")
    @ApiModelProperty(value = "topic2")
    private String thirdTopic;

    /**
     * topic3
     */
    @TableField(value = "fourth_topic")
    @ApiModelProperty(value = "topic3")
    private String fourthTopic;

    /**
     * 日志的常规数据(16进制)
     */
    @TableField(value = "log_data")
    @ApiModelProperty(value = "日志的常规数据(16进制)")
    private String logData;

    /**
     * 确认状态(10-未确认；20-有效；21-无效)
     */
    @TableField(value = "confirm_status")
    @ApiModelProperty(value = "确认状态(10-未确认；20-有效；21-无效)")
    private Integer confirmStatus;

    /**
     * 处理状态(10未处理 20已处理)
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value = "处理状态(10未处理 20已处理)")
    private Integer status;

    /**
     * 区块高度
     */
    @TableField(value = "block_number")
    @ApiModelProperty(value = "区块高度")
    private Long blockNumber;

    /**
     * 所在区块时间戳
     */
    @TableField(value = "block_timestamp")
    @ApiModelProperty(value = "所在区块时间戳")
    private Date blockTimestamp;

    @TableField(value = "method_id")
    @ApiModelProperty(value = "")
    private String methodId;

    @TableField(value = "create_by")
    @ApiModelProperty(value = "")
    private String createBy;

    @TableField(value = "create_date")
    @ApiModelProperty(value = "")
    private Date createDate;

    @TableField(value = "update_date")
    @ApiModelProperty(value = "")
    private Date updateDate;
}