package com.unknown.chainevent.common.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.unknown.chainevent.common.mapper")
public class AutowireConfiguration {
}
