package com.unknown.chainevent.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.unknown.chainevent.common.entity.ContractConfig;

public interface ContractConfigMapper extends BaseMapper<ContractConfig> {
    int insertSelective(ContractConfig record);

    int updateByPrimaryKeySelective(ContractConfig record);
}