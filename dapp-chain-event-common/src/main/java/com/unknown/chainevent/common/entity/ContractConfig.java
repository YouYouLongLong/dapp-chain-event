package com.unknown.chainevent.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * NFT合约配置
 */
@ApiModel(description = "NFT合约配置")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_contract_config")
public class ContractConfig {
    @TableId(value = "contract_config_id", type = IdType.AUTO)
    @ApiModelProperty(value = "")
    private Long contractConfigId;

    @TableField(value = "chain_config_id")
    @ApiModelProperty(value = "")
    private Long chainConfigId;

    /**
     * 链符号
     */
    @TableField(value = "chain_symbol")
    @ApiModelProperty(value = "链符号")
    private String chainSymbol;

    /**
     * NFT合约地址
     */
    @TableField(value = "contract_address")
    @ApiModelProperty(value = "NFT合约地址")
    private String contractAddress;

    /**
     * 合约类型：10：ERC20合约 20：REC721合约 30：其他合约
     */
    @TableField(value = "contract_type")
    @ApiModelProperty(value = "合约类型：10：ERC20合约 20：REC721合约 30：其他合约")
    private Integer contractType;

    /**
     * 名称
     */
    @TableField(value = "`name`")
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 符号
     */
    @TableField(value = "symbol")
    @ApiModelProperty(value = "符号")
    private String symbol;

    /**
     * 合约配置状态(10启用 20禁用)
     */
    @TableField(value = "contract_status")
    @ApiModelProperty(value = "合约配置状态(10启用 20禁用)")
    private Integer contractStatus;

    /**
     * 拓展字段1
     */
    @TableField(value = "expand1")
    @ApiModelProperty(value = "拓展字段1")
    private String expand1;

    /**
     * 拓展字段2
     */
    @TableField(value = "expand2")
    @ApiModelProperty(value = "拓展字段2")
    private String expand2;

    /**
     * 拓展字段3
     */
    @TableField(value = "expand3")
    @ApiModelProperty(value = "拓展字段3")
    private String expand3;

    @TableField(value = "create_by")
    @ApiModelProperty(value = "")
    private String createBy;

    @TableField(value = "create_date")
    @ApiModelProperty(value = "")
    private Date createDate;

    @TableField(value = "update_by")
    @ApiModelProperty(value = "")
    private String updateBy;

    @TableField(value = "update_date")
    @ApiModelProperty(value = "")
    private Date updateDate;
}