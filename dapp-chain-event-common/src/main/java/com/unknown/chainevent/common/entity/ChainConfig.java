package com.unknown.chainevent.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * NFT链配置
 */
@ApiModel(description = "NFT链配置")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_chain_config")
public class ChainConfig {
    @TableId(value = "chain_config_id", type = IdType.AUTO)
    @ApiModelProperty(value = "")
    private Long chainConfigId;

    /**
     * rpc地址
     */
    @TableField(value = "rpc_address")
    @ApiModelProperty(value = "rpc地址")
    private String rpcAddress;

    /**
     * 部署链,部署在那条链上,如ETH,bsc测试链等
     */
    @TableField(value = "chain_symbol")
    @ApiModelProperty(value = "部署链,部署在那条链上,如ETH,bsc测试链等")
    private String chainSymbol;

    /**
     * 区块高度，初始化为部署的区块高度。
     */
    @TableField(value = "block_number")
    @ApiModelProperty(value = "区块高度，初始化为部署的区块高度。")
    private Long blockNumber;

    /**
     * 链的chain id
     */
    @TableField(value = "chain_id")
    @ApiModelProperty(value = "链的chain id")
    private Long chainId;

    /**
     * 链状态(10启用 20禁用)
     */
    @TableField(value = "chain_status")
    @ApiModelProperty(value = "链状态(10启用 20禁用)")
    private Integer chainStatus;

    /**
     * 确认区块数
     */
    @TableField(value = "confirm_block")
    @ApiModelProperty(value = "确认区块数")
    private Integer confirmBlock;

    /**
     * 不信任区块数（扫块区块为：最新快-distrust_block）
     */
    @TableField(value = "distrust_block")
    @ApiModelProperty(value = "不信任区块数（扫块区块为：最新快-distrust_block）")
    private Integer distrustBlock;

    /**
     * 重复扫描旧的区块数
     */
    @TableField(value = "scan_old_block")
    @ApiModelProperty(value = "重复扫描旧的区块数")
    private Integer scanOldBlock;

    @TableField(value = "create_by")
    @ApiModelProperty(value = "")
    private String createBy;

    @TableField(value = "create_date")
    @ApiModelProperty(value = "")
    private Date createDate;

    @TableField(value = "update_by")
    @ApiModelProperty(value = "")
    private String updateBy;

    @TableField(value = "update_date")
    @ApiModelProperty(value = "")
    private Date updateDate;
}