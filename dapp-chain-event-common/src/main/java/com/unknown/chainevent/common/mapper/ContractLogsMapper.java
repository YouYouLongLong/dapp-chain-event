package com.unknown.chainevent.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.unknown.chainevent.common.entity.ContractLogs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ContractLogsMapper extends BaseMapper<ContractLogs> {

    int insertSelective(ContractLogs record);

    int updateByPrimaryKeySelective(ContractLogs record);

    /**
     * 根据hash和日志索引获取条数
     *
     * @param hash
     * @param index
     * @return
     */
    int countByHashAndIndex(@Param("hash") String hash, @Param("index") Long index);

    /**
     * 更新状态为已处理
     *
     * @param id
     * @return
     */
    int updateStatusSuccess(@Param("id") Long id);

    /**
     * 更新确认状态
     *
     * @param hash
     * @param status
     * @param oldStatus
     * @return
     */
    int updateConfirmStatus(@Param("hash") String hash, @Param("status") Integer status, @Param("oldStatus") Integer oldStatus);

    List<ContractLogs> selectLogsByContractName(@Param("firstTopic") String firstTopic, @Param("contractAddress") String contractAddress,
        @Param("contractName") String contractName, @Param("targetTable") String targetTable, @Param("limit") Integer limit);
}