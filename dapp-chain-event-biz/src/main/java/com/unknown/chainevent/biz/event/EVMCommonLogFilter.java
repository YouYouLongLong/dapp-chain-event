package com.unknown.chainevent.biz.event;

import com.unknown.chainevent.biz.service.ChainConfigService;
import com.unknown.chainevent.biz.service.ContractConfigService;
import com.unknown.chainevent.biz.service.ContractLogsService;
import com.unknown.chainevent.common.entity.ChainConfig;

import java.io.IOException;

/**
 * EVM类链公共事件扫描
 * 直接使用 BaseLogFilter的功能
 */
public class EVMCommonLogFilter extends BaseLogFilter{

    public EVMCommonLogFilter(ChainConfigService chainConfigService, ContractConfigService contractConfigService, ContractLogsService contractLogsService, ChainConfig chainConfig) throws IOException {
        super(chainConfigService, contractConfigService, contractLogsService, chainConfig);
    }

}
