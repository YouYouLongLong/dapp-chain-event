package com.unknown.chainevent.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.unknown.chainevent.common.entity.ContractLogs;
import java.util.List;

public interface ContractLogsService extends IService<ContractLogs> {


    int insertSelective(ContractLogs record);

    int updateByPrimaryKeySelective(ContractLogs record);

    /**
     * log是否存在
     *
     * @param hash
     * @param index
     * @return
     */
    public boolean existsByHashAndIndex(String hash, Long index);

    /**
     * 处理交易确认
     */
    void processTransactionConfirm();

    /**
     * 更新日志状态为已完成
     *
     * @param id
     */
    public void updateStatusSuccess(Long id);

    /**
     * 根据合约名称获取日志列表
     * @param firstTopic 事件签名
     * @param contractAddress 合约地址
     * @param contractName 合约名称
     * @param targetTable 目标表
     * @return
     */
    List<ContractLogs> selectLogsByContractName(String firstTopic, String contractAddress, String contractName, String targetTable,Integer limit);

    List<ContractLogs> selectLogsByContractName(String firstTopic, String contractName, String targetTable, Integer limit);

    List<ContractLogs> selectLogsByContractName(String firstTopic, String contractName, String targetTable);
}
