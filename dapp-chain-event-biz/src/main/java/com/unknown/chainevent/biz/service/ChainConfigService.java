package com.unknown.chainevent.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.unknown.chainevent.common.entity.ChainConfig;
import org.cloud.exception.BusinessException;
import org.cloud.vo.CommonApiResult;

import java.io.IOException;
import java.util.List;

public interface ChainConfigService extends IService<ChainConfig>{


    int insertSelective(ChainConfig record);

    int updateByPrimaryKeySelective(ChainConfig record);

    /**
     * 更新最后区块
     * @param id
     * @param block
     * @return
     */
    public int updateLastBlock(Long id, Long block);

    /**
     * 获取链列表
     * @param onlyValid 是否仅获取有效
     * @return
     */
    List<ChainConfig> getChainList(boolean onlyValid);

    CommonApiResult<?> updateChain(ChainConfig config) throws BusinessException, IOException;

    CommonApiResult<?>  addChain(ChainConfig config) throws BusinessException, IOException;

    CommonApiResult<?>  updateStatus(Long chainConfigId, Integer status) throws BusinessException;
}
