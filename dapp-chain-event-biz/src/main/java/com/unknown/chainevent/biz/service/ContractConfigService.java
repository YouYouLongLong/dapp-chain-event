package com.unknown.chainevent.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.unknown.chainevent.common.entity.ContractConfig;
import org.cloud.exception.BusinessException;
import org.cloud.vo.CommonApiResult;

public interface ContractConfigService extends IService<ContractConfig>{


    int insertSelective(ContractConfig record);

    int updateByPrimaryKeySelective(ContractConfig record);

    CommonApiResult<?> updateContract(ContractConfig config) throws BusinessException;

    CommonApiResult<?> addContract(ContractConfig config) throws BusinessException;

    CommonApiResult<?> updateStatus(Long contractConfigId, Integer status) throws BusinessException;

    ContractConfig getBySymbol(String symbol);
}
